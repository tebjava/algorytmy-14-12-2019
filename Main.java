/*
 * DO ZROBIENIA:
 * - wyeliminowac problem przetawienia szyku wielomianu (np. zamiast 2x^2+3x-2 -> 3x-2+2x^2)
 * - obsluzyc pozostale wyjatki i ewentualne bledy
 * - "nauczyc" program osblugi wielomianow wyzszego stopnia
 * 
 * PROGRAM + ALGORYTM
 * - napisac program wyliczajacy pochodne I i II stopnia z podanych wielomianow
 * - program do RPN (Reverse Polish Notation); wersja prosta - uzytkownik podaje RPN
 * - wersja optymalna - zamieniamy notacje tradycyjna na RPN (i obliczamy)
 * 
 * Dostarczenie - link do gitlaba na adres piotr_dobosz@int.pl
 */

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Main().wykonajDzialanie();
	}

	
	String wzor="";
	double a=0;
	double b=0;
	double c=0;
	double d=0;
	double x1=0;
	double x2=0;
	
	@SuppressWarnings("resource")
	void wykonajDzialanie() {
		System.out.print("Podaj swoj wielomian stopnia 2: ");
		wzor=new Scanner(System.in).nextLine();
		znajdzZmienne();
	}
	
	void znajdzZmienne( ) {
		wzor=wzor.replaceAll("[\\\\#!@%`\\*()_=~{}\\[\\]\\?/<>,.|]?", "");
		//System.out.println(wzor);
		wzor=wzor.replaceAll("[a-zA-Z&&[^xX]]?", "");
		wzor=wzor.replace(" ","");
		//System.out.println(wzor);
		//System.out.print(wzor.split("[\\^]{1}[^2]{1}").length);
		if (wzor.split("[\\^]{1}[^2]{1}").length==2 || wzor.indexOf('^')==-1) {
		//if (wzor.split("\\^2").length!=2) {
			System.out.print(wzor.split("\\^[^2]{1}")[0]);
			System.out.println("Podales zly rodzaj wielomianu! Koncze program!");
			System.exit(-1);
		}
		String tmp[] = wzor.split("\\*?x?X?[xX]{1}\\^?2?");
		//for (String val : tmp) {
			//System.out.println(Double.valueOf(val));
		//}
		a = Double.valueOf(tmp[0]);
		b = Double.valueOf(tmp[1]);
		c = Double.valueOf(tmp[2]);
		if  (a==0) {
			System.out.println("Wartosc a nie moze byc zerowa! Koncze program!");
			System.exit(-1);
		}
		d = b*b - 4*a*c;
		x1 = (-b-d)/2*a;
		System.out.println("Pierwsze miejsce zerowe to: " + x1);
		if(d!=0) {
			x2 = (-b+d)/2*a;
			System.out.println("Drugie miejsce zerowe to: " + x2);
			
		}
		
	}
	
}
                                                                                            